module gitlab.com/quocbang/go-webrtc/server

go 1.20

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	go.uber.org/zap v1.26.0
)

require go.uber.org/multierr v1.10.0 // indirect
