package main

import (
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/quocbang/go-webrtc/server/server"
)

func init() {
	logger, _ := zap.NewProduction()
	zap.NewStdLog(logger)
}

func main() {
	http.HandleFunc("/create", server.CreateRoomRequestHandler)
	http.HandleFunc("/join", server.JoinRoomRequestHandler)
	http.HandleFunc("/room/list", server.ListRoomConnections)

	log.Println("Starting Server on Port 8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
