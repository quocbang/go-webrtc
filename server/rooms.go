package server

import (
	"fmt"
	"log"
	"sync"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

type Participant struct {
	Host bool
	Conn *websocket.Conn
}

type RoomMap struct {
	Mutex sync.RWMutex
	Map   map[string][]Participant
}

// Get is get room by room id
func (r *RoomMap) Get(roomID string) []Participant {
	r.Mutex.RLock()
	defer r.Mutex.RUnlock()

	return r.Map[roomID]
}

// CreateRoom is create room to map
func (r *RoomMap) CreateRoom() string {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	roomID := uuid.NewString()
	r.Map[roomID] = []Participant{}
	return roomID
}

// InsertIntoRoom is insert into room to map.
func (r *RoomMap) InsertIntoRoom(roomID string, host bool, conn *websocket.Conn) error {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if participant, ok := r.Map[roomID]; ok {
		p := Participant{host, conn}
		log.Printf("Insert into room with room id: %s", roomID)
		r.Map[roomID] = append(participant, p)
	} else {
		return fmt.Errorf("room id do not exist")
	}

	return nil
}

// DeleteRoom is delete room in the map.
func (r *RoomMap) DeleteRoom(roomID string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	delete(r.Map, roomID)
}
