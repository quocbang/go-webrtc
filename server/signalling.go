package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// AllRooms in map.
var AllRooms RoomMap

// Initialize map
func init() {
	AllRooms.Map = make(map[string][]Participant)
}

// CreateRoomRequestHandler a room and return roomID.
func CreateRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	roomID := AllRooms.CreateRoom()

	type resp struct {
		RoomID string
	}

	log.Println(roomID)

	json.NewEncoder(w).Encode(resp{RoomID: roomID})
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type broadcastMsg struct {
	Message map[string]interface{}
	RoomID  string
	Client  *websocket.Conn
}

var broadcast = make(chan broadcastMsg)

func broadcaster() {
	for {
		msg := <-broadcast

		for _, client := range AllRooms.Map[msg.RoomID] {
			go func(client Participant) {
				err := client.Conn.WriteJSON(msg.Message)
				if err != nil {
					log.Println(err)
					client.Conn.Close()
				}
			}(client)
		}
	}
}

// JoinRoomRequestHandler will join the client in a particular room.
func JoinRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	roomID, ok := r.URL.Query()["roomID"]
	if !ok {
		log.Println("missing roomID")
		return
	}
	userID, ok := r.URL.Query()["userID"]
	if !ok {
		log.Println("missing userID")
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("web socket upgrade error: %v", err)))
		return
	}

	err = AllRooms.InsertIntoRoom(roomID[0], false, ws)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	go broadcaster()

	for {
		var msg broadcastMsg

		t, p, err := ws.ReadMessage()
		if t == -1 {
			continue
		}
		if err != nil {
			log.Fatal("read error: ", err)
		}
		msg.Client = ws
		msg.RoomID = roomID[0]
		msg.Message = map[string]interface{}{
			userID[0]: string(p),
		}

		broadcast <- msg
	}
}

func ListRoomConnections(w http.ResponseWriter, r *http.Request) {
	roomID, ok := r.URL.Query()["roomID"]
	if !ok {
		w.Write([]byte("missing room id"))
	}

	p := AllRooms.Get(roomID[0])

	log.Println(p)
}
